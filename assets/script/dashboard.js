// Dashboard js
$(document).ready(function(){
    // Dataepicker init
    $('.datepicker').datepicker();

    // Select2 init
    $('.select2_enabled').select2();

    // Feather icon init
    feather.replace()

    // Popper js
    $('[data-toggle="tooltip"]').tooltip()

    // WOW js init
    new WOW().init();
});
