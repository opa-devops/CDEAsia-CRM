/**********************************************
    Dynamic field handler for the forms
**********************************************/



// Additional information add for Item-and-price-list page
$(document).ready(function () {
  var maxField = 4; //Input fields increment limitation
  var addButton = $('.addAditionalInfo'); //Add button selector
  var wrapper = $('.addinward'); //Input field wrapper
  var fieldHTML =
      '<div class="row"> <div class="col-4"> <div class="form-group"> <input type="Text" class="form-control" id="usr"> </div></div><div class="col-4"> <div class="form-group"> <input type="Text" class="form-control" id="usr"> </div></div><div class="col-3"> <div class="form-group"> <input type="Text" class="form-control" id="usr"> </div></div><div class="col-1"> <a href="javascript:void(0);" class="btn btn-danger removeAditionalInfo">-</a> </div></div>'; //New input field html 
  var x = 1; //Initial field counter is 1

  //Once add button is clicked
  $(addButton).click(function () {
      //Check maximum number of input fields
      if (x < maxField) {
          x++; //Increment field counter
          $(wrapper).append(fieldHTML); //Add field html
      }
  });

  //Once remove button is clicked
  $(wrapper).on('click', '.removeAditionalInfo', function (e) {
      e.preventDefault();
      $(this).parent().parent().remove(); //Remove field html
      x--; //Decrement field counter
  });
});

// go to back page
function goBack() {
    window.history.back();
  }